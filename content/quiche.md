---
title: "Quiche"
date: 2019-06-29T14:58:38Z
draft: false
---

Flan parisien
-------------

Ingrédients pour la pâte brisée

* 200 g de farine
* 20 g de sucre glace
* 1 pincée de sel
* 100 g de beurre
* 2 jaunes d'oeuf
* 30 g d'eau

Pétrir tous les ingrédients


Ingrédients pour le flan

* 1/2 l de lait
* 125 g de farine
* 1 gousse de vanille
